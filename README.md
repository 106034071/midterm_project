# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : ChatRoom
* Key functions (add/delete)
    1. [Private chat room] : User can create a private room which only can be read/write by himself/herself or friends that has been added to the room 
    2. [Public room] : Every user can chat in this room
    3. [Add friends] : Can add many friends to a private room
* Other functions (add/delete)
   

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-chat-room-3a0fa.web.app

# Components Description : 
**1.[Membership Mechanism]** : 
    從主頁點選login後會可以選擇直接登入或是用google帳號登入(註冊)，或是可以按Create an account註冊自己想要的email與密碼，註冊成功後會自動登入並且回主頁

**2. [Host on your Firebase page]** : 已經deploy到firebase上可以點擊上方網址進入

**3. [Database read/write]** : 
    在choose room的地方會根據該使用者有的room做讀取，也可以謝入新的room，private room內會讀取歷史訊息，也可以寫入新訊息，而public room只要是有登入的使用者都能讀取和寫入
**4. [RWD]** : 主要用bootstrap搞的

**5. [Topic Key Functions]** : 
     在home page點擊"Rooms"就可以進到choose page，choose page可以選擇以前就建立的private room或是在文字框框中打入想新增的room name再按下"add"就能新增一個private room
     若是想要新增朋友進到自己的private room，可以在文字框框裡打上朋友的email，按下"Add friend"，朋友就會被加到這個private room
     
**6. [Sign Up/In with Google]**:
    有，在登入的時候選Sign in with Google即可
    
**7.[Add Chrome notification]**:
    當別人傳訊息過來的時候，會顯示傳送者的email與訊息內容
    
**8.[Use CSS animation]**:
    只有在login page時，如果按下Create an account會有滑動切換的動畫效果
    
**9.[Deal with messages when sending html code]**:
    用<xmp></xmp>去處理
    
# Other Functions Description : 
**1. [Chats]** :
    每次有訊息傳入都會自動在最下方 
    
**2. [Public]** : 
    每個有登入的使用者都能在Public room聊天
    
**3. [messages]** : 
    別人傳的訊息跟自己船的訊息會用顏色分開，如果是別人傳的訊息會在訊息內容上方加上傳送人的*email

## Security Report (Optional)
1.輸入html code時不會變成網頁內容

2.在private room裡只有房間創建者或是被邀請人才有權讀取/傳送該房間訊息

3.即使創建了很多同名字的private room，訊息也不會混在一起
