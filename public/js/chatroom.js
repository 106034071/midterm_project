var flag = 1;
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('userNow');
        // Check user login

        if (user) {
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (permission) {
                });
            }

            var temp = document.getElementById('room_name');
            var roomRef = firebase.database().ref('room');
            roomRef.once('value', function (snapshot) {
                var child = snapshot.val();
                var aa = child.room;
                console.log(child.room_key)
                temp.innerHTML = aa;
                tempName = aa;
                //console.log(tempName)

                var submit_btn = document.getElementById('Submitbtn');
                var comment = document.getElementById('message');
                var room_name = document.getElementById('room_name');
                firebase.database().ref("Private/" + child.room_key /*+ child.Uid*/ + "/room_name").set({ name: aa });
                var flag = 1;
                var members = firebase.database().ref("Private/" + child.room_key /*+ child.Uid */ + "/members");
                members.once('value', function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        if (user.email == childSnapshot.val().email) {
                            flag = 0;
                        }
                    });
                    if (flag) members.push({ email: user.email });
                });
                submit_btn.addEventListener('click', function () {
                    if (comment.value != "") {
                        var mes = {
                            user_email,
                            message: comment.value
                        }
                        //console.log(room_name.innerText);
                        firebase.database().ref("Private/" + child.room_key/* + child.Uid*/ + "/messages").push(mes);
                        //firebase.database().ref('cc').push(mes);
                        message.value = "";
                    }
                });
                var addFriend_btn = document.getElementById('addFriend');
                var friend_email = document.getElementById('f_email');
                var members = firebase.database().ref("Private/" + child.room_key /*+ child.Uid*/ + "/members");
                addFriend_btn.addEventListener('click', function () {
                    flag = 1;
                    console.log("hello")
                    if (friend_email.value != "") {
                        members.once('value', function (snapshot) {
                            snapshot.forEach(function (childSnapshot) {
                                if (friend_email.value == childSnapshot.val().email) {
                                    flag = 0;
                                }
                            });
                            if (flag) {
                                members.push({ email: friend_email.value });
                            }
                            friend_email.value = "";
                        });
                    }
                });
            });


            var check_old = 0;
            var cnt;
            
            var roomRef = firebase.database().ref('room');
           
            roomRef.once('value', function (snapshot) {
                var child = snapshot.val();
                var mesList = document.getElementById('messageList');
                console.log(child.key);
                var postRef = firebase.database().ref("Private/" + child.room_key /*+ child.Uid + child.count*/ + "/messages");
                postRef.once('value', function (snapshot) {
                    cnt = snapshot.numChildren();
                });
                postRef.on('child_added', function (snapshot) {
                    check_old++;
                    var data = snapshot.val();
                    if (snapshot.key != "members") {
                        if (data.user_email == user.email) {
                            var head = '<div class="text-right mymes rounded">';
                            var post_mes = '<xmp>' + data.message + '</xmp></div>';
                            mesList.innerHTML += head + post_mes;
                        } else {
                            var head = '<div class="text-left mes rounded">' + data.user_email;
                            var post_mes = ' : <xmp>' + data.message + '</xmp></div>';
                            mesList.innerHTML += head + post_mes;
                            var notifyConfig = {
                                body: data.message
                            };
                            Notification.requestPermission(function (permission) {
                                if (permission === 'granted' && check_old > cnt) {
                                    var notification = new Notification(data.user_email, notifyConfig);
                                }
                            });
                        }
                        mesList.scrollTop = mesList.scrollHeight;
                    }
                });
            });

            user_email = user.email;
            menu.innerHTML = '<li class="nav-item active"><a class="navbar-brand">' + user.email + '</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="../choose.html">Chats</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="#" id="logout-btn">Logout</a></li>';
            var btn = document.getElementById("logout-btn");
            btn.addEventListener('click', function (r) {
                firebase.auth().signOut()
                    .then(function () {
                        window.location.assign("../index.html");
                        console.log("sign out!");
                    })
                    .catch(function (error) {
                        var errorMessage = error.message;
                        console.log(errorMessage);
                    })
            });

        } else {
            menu.innerHTML = '<a class="navbar-brand" href="login.html">Login</a>';
        }
    });

}




window.onload = function () {
    init();
};