function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('userNow');
        // Check user login
        User = user;
        if (user) {
            user_email = user.email;
            menu.innerHTML = '<li class="nav-item active"><a class="navbar-brand">' + user.email + '</a></li>' + '<li class="nav-item active"><a class="navbar-brand" id="public" href="#">Public</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="#">Chats</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="#" id="logout-btn">Logout</a></li>';
            var btn = document.getElementById("logout-btn");
            btn.addEventListener('click', function (r) {
                firebase.auth().signOut()
                    .then(function () {
                        window.location.assign("../index.html");
                        console.log("sign out!");
                    })
                    .catch(function (error) {
                        var errorMessage = error.message;
                        console.log(errorMessage);
                    })
            });

            var btn = document.getElementById("public");
            btn.addEventListener('click', function (r) {
                window.location.assign("../Public.html");
                console.log("sign out!");
            });

            var listRef = firebase.database().ref("Private");
            var room_list = document.getElementById('roomList');
            //console.log(listRef)
            var cnt = 0;
            listRef.once('value', function (snapshot) {
                cnt = snapshot.numChildren();
                //console.log(cnt);
            });

            listRef.on('child_added', function (snapshot) {
                //console.log(snapshot.key)
                var getRoom_name = firebase.database().ref('Private/' + snapshot.key + '/room_name');
                var avai_user = firebase.database().ref('Private/' + snapshot.key + '/members');
                avai_user.on('child_added', function (ss) {

                    //console.log(ss.val().email)
                    if (user.email == ss.val().email) {
                        getRoom_name.on('child_added', function (snap) {
                            //console.log(snap.val())
                            var btn = '<div class="pt-2"><button class="btn btn-dark btn-block Rbtn" name=' + snapshot.key + '>' + snap.val() + '</button></div>';
                            room_list.innerHTML += btn;
                            room_list.scrollTop = room_list.scrollHeight;
                        });
                    }
                });
            });



            var addbtn = document.getElementById('Addbtn');
            var room_name = document.getElementById('name');

            addbtn.addEventListener('click', function () {
                //console.log(cnt);
                if (room_name.value != "") {
                    var temp = firebase.database().ref("Private/").push('');
                    //console.log(temp.key)
                    var mes = {
                        email: user.email,
                    }
                    try {
                        firebase.database().ref("Private/" + temp.key /*+ user.uid*/ + '/members').push(mes);
                    } catch (e) {
                        room_name.value = "";
                        alert("Room name can't contain . # $ [  ]")
                    }

                    mes = {
                        name: room_name.value
                    }
                    try {
                        firebase.database().ref("Private/" + temp.key /*+ user.uid*/ + '/room_name').set(mes)
                    } catch (e) {
                        room_name.value = "";
                        alert("Room name can't contain . # $ [  ]")
                    }
                    addbtn.name = temp.key;
                    //console.log(addbtn.name);
                    room_name.value = "";
                }
            });


            $('body').on('click', 'button.Rbtn', function () {
                //$(this).attr("name")
                console.log($(this).attr("name"));
                var mes = {
                    room: $(this).html(),
                    room_key: $(this).attr("name")
                }

                firebase.database().ref('room').set(mes);
                window.location.assign("../chatRoom.html");
            });
        } else {
            menu.innerHTML = '<a class="navbar-brand" href="login.html">Login</a>';
        }
    });


}

window.onload = function () {
    init();
};