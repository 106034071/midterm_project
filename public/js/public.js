var flag = 1;
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('userNow');
        // Check user login

        if (user) {
            if (!('Notification' in window)) {
                console.log('This browser does not support notification');
            }
            if (Notification.permission === 'default' || Notification.permission === 'undefined') {
                Notification.requestPermission(function (permission) {
                });
            }



            menu.innerHTML = '<li class="nav-item active"><a class="navbar-brand">' + user.email + '</a></li>' + '<li class="nav-item active"><a class="navbar-brand" id="public" href="#">Public</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="choose.html">Chats</a></li>' + '<li class="nav-item active"><a class="navbar-brand" href="#" id="logout-btn">Logout</a></li>';

            var submit_btn = document.getElementById('Submitbtn');
            var comment = document.getElementById('message');

            submit_btn.addEventListener('click', function () {
                console.log("xx")
                if (comment.value != "") {
                    var mes = {
                        user_email,
                        message: comment.value
                    }
                    firebase.database().ref("Public/messages").push(mes);
                    message.value = "";
                }
            });


            var mesList = document.getElementById('messageList');

            var postRef = firebase.database().ref("Public/messages");
            var check_old = 0;
            var cnt;
            postRef.once('value', function (snapshot) {
                cnt = snapshot.numChildren();
            });
            postRef.on('child_added', function (snapshot) {

                check_old++;
                console.log()
                var data = snapshot.val();
                if (data.user_email == user_email) {
                    var head = '<div class="text-right mymes rounded">';
                    var post_mes = '<xmp>' + data.message + '</xmp></div>';
                    mesList.innerHTML += head + post_mes;
                } else {
                    var head = '<div class="text-left mes rounded">' + data.user_email;
                    var post_mes = ' : <xmp>' + data.message + '</xmp></div>';
                    mesList.innerHTML += head + post_mes;
                    var notifyConfig = {
                        body: data.message
                    };
                    Notification.requestPermission(function (permission) {
                        if (permission === 'granted' && check_old > cnt) {
                            var notification = new Notification(data.user_email, notifyConfig);
                        }
                    });
                }

                mesList.scrollTop = mesList.scrollHeight;

            });


            user_email = user.email;
            var btn = document.getElementById("logout-btn");
            btn.addEventListener('click', function (r) {
                firebase.auth().signOut()
                    .then(function () {
                        window.location.assign("../index.html");
                        console.log("sign out!");
                    })
                    .catch(function (error) {
                        var errorMessage = error.message;
                        console.log(errorMessage);
                    })
            });

        } else {
            menu.innerHTML = '<a class="navbar-brand" href="login.html">Login</a>';
        }
    });

}




window.onload = function () {
    init();
};